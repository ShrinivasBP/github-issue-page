import React from "react"

import "./index.css"

class Issues extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            issue: this.props.issuesTitle,
            delay: 0,
            view: this.props.view,
            time: this.props.date,
            url: this.props.url,
            delayByHours: false,
            delayByDays: false
        }
    }

    componentDidMount() {

        setInterval(() => {
            this.setState((prevState) => {

                let delayHour = false
                let delayDay = false

                let issueOpenMin = (new Date(prevState.time) / (1000 * 60))
                let currentMin = Date.now() / (1000 * 60)

                let diffinMin = currentMin - issueOpenMin

                if (diffinMin > 60 & diffinMin < 1440) {
                    diffinMin = (diffinMin / 60)
                    // console.log(diffinMin)
                    delayHour = true
                }
                else if (diffinMin > 1440 & diffinMin < 43200) {
                    diffinMin = (diffinMin / (60 * 24))
                    delayDay = true
                }
                // console.log(issueOpenMin, currentMin, diffinMin)
                return {
                    delay: Math.floor(diffinMin),
                    delayByHours: delayHour,
                    delayByDays: delayDay
                }
            })
        }, 1000)
    }

    handelClick = () => {
        document.location = (this.state.url)
    }
    render() {
        return (
            <div class="issue-wrapper">
                <div class="title-wrapper">
                    <svg class="octicon octicon-issue-opened UnderlineNav-octicon d-none d-sm-inline" height="16" viewBox="0 0 16 16" version="1.1" width="16" aria-hidden="true" fill="green"><path fill-rule="evenodd" d="M8 1.5a6.5 6.5 0 100 13 6.5 6.5 0 000-13zM0 8a8 8 0 1116 0A8 8 0 010 8zm9 3a1 1 0 11-2 0 1 1 0 012 0zm-.25-6.25a.75.75 0 00-1.5 0v3.5a.75.75 0 001.5 0v-3.5z"></path></svg>
                    <h2 onClick={this.handelClick} className="issue-title"> {this.state.issue}</h2>
                </div>
                <p className="issue-views"> #{this.state.view} opened {this.state.delay} {this.state.delayByHours ? "Hours " : this.state.delayByDays ? "Days " : "minutes "}ago by {this.state.name}</p>
            </div>
        )
    }
}

export default Issues