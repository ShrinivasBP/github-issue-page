import React from "react"

import Issues from "./Issues"

import "./index.css"

class IssuesContainer extends React.Component {
    constructor() {
        super()

        this.state = {
            data: [],
            issueTitle: "",
            view: 0,
            url: ""
        }
    }
    componentDidMount() {
        fetch("https://api.github.com/repos/rails/rails/issues")
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                const issuesData = data.map((item) => {
                    console.log(item.number)
                    return (<Issues key={item.id} name={item.user.login} date={item.created_at} issuesTitle={item.title} view={item.number} url={item.html_url} />)
                })
                this.setState(() => {
                    return {
                        data: issuesData
                    }
                })
            })
    }
    render() {
        return (
            <div className="issues-container">

                {this.state.data}
            </div>
        )
    }
}

export default IssuesContainer