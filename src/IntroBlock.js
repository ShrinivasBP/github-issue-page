import React from "react";

import "./index.css";

class IntroBlock extends React.Component {
    render() {
        return (
            <div className="intro-wrapper">
                <button id="close-intro">X</button>
                <h1>Learn Git and GitHub without any code!</h1>
                <h3>
                    Using the Hello World guide, you’ll start a branch, write comments,
                    and open a pull request.
                </h3>
                <button id="read-the-guide-button">Read the guide</button>
            </div>
        );
    }
}

export default IntroBlock;
