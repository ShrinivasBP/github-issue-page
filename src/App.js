import React from "react"
// import logo from './logo.svg';
import Header from "./Header"
import IntroBlock from "./IntroBlock"
import NavSection from "./NavSection"
import IssuesContainer from "./IssuesContainer"
import Footer from "./Footer"
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div class="container">
        <Header />
        <IntroBlock />
        <NavSection />
        <IssuesContainer />
        <Footer />
      </div>
    );
  }
}

export default App;
